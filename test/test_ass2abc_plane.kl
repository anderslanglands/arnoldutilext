// A slightly modified version of the sphere example from arnoldpedia
// tests basic node functionality
require AlembicWrapper, FileIO, Geometry, ArnoldUtil, Arnold, Math;

operator entry() 
{
   AiBegin();

   // load the ass file
   if (AiASSLoad("test/test_asswrite_plane.ass", AI_NODE_ALL) != 0)
   {
      setError('Could not load ass file test/test_asswrite_plane.ass');
      return;
   }

   FilePath path = FilePath("test/test_ass2abc_plane.abc");
   AlembicArchiveWriter archive(path.string());
   if (!archive.valid())
   {
      setError("Could not create Alembic archive " + path.string());
      return;
   }

   // Get a universe iterator for all shapes
   AtNodeIterator iter = AiUniverseGetNodeIterator(AI_NODE_SHAPE);
   while (!AiNodeIteratorFinished(iter))
   {
      AtNode node = AiNodeIteratorGetNext(iter);
      String name = AiNodeGetName(node);
      AtNodeEntry nentry = AiNodeGetNodeEntry(node);
      String typename = AiNodeEntryGetTypeName(nentry);

      // only handle meshes for now
      if (!AiNodeIs(node, "polymesh")) continue;

      report("name: " + name);

      // The Arnold universe has no concept of a transform hierarchy, so to put an Xfo in, we'll grab the "shape name". 
      // If the object came from Maya or Alembic, then the full path will be baked into the Arnold node name and the 
      // separation parameters will be '|' or '/'. We'll want to strip off the final part to create the new "shape name".
      // This wil of course lose the hierachy info, but we've already lost the separated transforms so it's not like we 
      // can reconstruct it anyway.
      String toks[];
      toks = name.splitAny("|/");
      name = toks[toks.size()-1];
      report("new name: " + name);

      String xfo_path = "/" + name + "Xfo";
      String mesh_path = xfo_path + "/" + name;

      report("xfo : " + xfo_path);
      report("mesh: " + name);

      Mat44 xfo = AiNodeGetMatrix(node, "matrix");
      xfo = xfo.transpose(); // < Arnold's convention is the opposite to Fabric's

      PolygonMesh mesh = ArnoldUtilConvertPolygonMesh(node);

      
      AlembicXformWriter xform_writer = archive.addXform(xfo_path, Alembic_kConstantTimeSampling);
      AlembicPolyMeshWriter mesh_writer = archive.addPolyMesh(mesh_path, Alembic_kConstantTimeSampling);

      xform_writer.writeSample(xfo);
      mesh_writer.writeSample(mesh);
   }

   archive.reset();

   AiEnd();
}