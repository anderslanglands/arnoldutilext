VERSION = 0.1.0

INSTALL_ROOT = ${HOME}/vfx/fabric/Exts/ArnoldUtil$(VERSION)

KL = 	ArnoldUtil.fpm.json \
		Arnold_polygonmesh.kl \
		Arnold_alembic.kl

install:
	mkdir -p $(INSTALL_ROOT)
	cp $(KL) $(INSTALL_ROOT)